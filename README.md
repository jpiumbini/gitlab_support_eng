## Full Documentation is in https://gitlab.com/jpiumbini/gitlab_support_eng/-/wikis/home
1 - To execute the script you will need superuser permission.<br>
2 - Set execute permission on file run.sh ```chmod +x run.sh```<br>
3 - Execute the script ```./run.sh```, this script will copy the files (check_user.sh and current_users.sh) to /usr/sbin<br>
4 - Edit the crontab e.g ```crontab -e``` and add the line:<br>
0 * * * * /usr/sbin/check_user.sh<b>
