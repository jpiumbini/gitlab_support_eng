#!/bin/bash

CUR_USERS_MD5=$(/usr/sbin/current_users.sh | md5sum | awk '{print $1}')
CUR_USERS_FILE=/var/log/current_users
USER_CHANGES_FILE=/var/log/user_changes

#check if file exist
if [ -f "$CUR_USERS_FILE" ]; then
	OLD_USERS_MD5=$(cat $CUR_USERS_FILE)
else
	echo "$CUR_USERS_MD5" > $CUR_USERS_FILE
fi


if ! [ "$CUR_USERS_MD5" = "$OLD_USERS_MD5" ]; then
	echo $(date) - 'CHANGES OCCURRED' >> $USER_CHANGES_FILE
	echo "$CUR_USERS_MD5" > $CUR_USERS_FILE
fi	
